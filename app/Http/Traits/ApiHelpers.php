<?php
namespace App\Http\Traits;

use Illuminate\Http\JsonResponse;

trait ApiHelpers
{

    protected function response($data, string $message = '', int $code = 200): JsonResponse
    {
        return response()->json([
            'status' => $code,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

}
