<?php

namespace App\Http\Middleware;

use App\Http\Traits\ApiHelpers;
use Closure;
use Illuminate\Http\Request;
use App\User;
use App\Usergroups;
use Illuminate\Support\Facades\Auth;

class AuthAdminRole
{

    use ApiHelpers;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->user()->authorizeRoles(['admin'])) {
            return $this->response(null, "Unauthorized.", 500);
        }

        return $next($request);
    }
}
