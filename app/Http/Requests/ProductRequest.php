<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed name
 * @property mixed description
 * @property mixed price
 * @property mixed sku
 * @property mixed subcategory_id
 * @property mixed category_id
 * @property mixed details
 */
class ProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "description" => "required",
            "price" => "required",
            "sku" => "required|unique:products",
            "subcategory_id" => "required|exists:subcategory_products,id",
            "category_id" => "required|exists:category_products,id",
            "details" => 'array|min:1',
        ];
    }
}
