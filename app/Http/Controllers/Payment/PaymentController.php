<?php


namespace App\Http\Controllers\Payment;
use App\Http\Controllers\Controller;
use Stripe;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    /**
     * success response method.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function checkout()
    {
        return view('payment.checkout');
    }

    public function paymentSave(Request $request)
    {

        Stripe\Stripe::setApiKey(env('STRIPE_API_SECRET'));
        Stripe\Charge::create ([
            "amount" => 100 * 100,
            "currency" => "usd",
            "source" => $request->stripeToken,
            "description" => "Test payment from itsolutionstuff.com."
        ]);

        return back();
    }
}
