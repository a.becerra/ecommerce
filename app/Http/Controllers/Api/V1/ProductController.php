<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Traits\ApiHelpers;
use App\Models\Product;
use App\Models\ProductDetails;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    use ApiHelpers;

    public function __construct()
    {
//        $this->middleware('auth:sanctum');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        try {
            return $this->response(Product::all(), "Success", 200);
        } catch (\Exception $e) {
            return $this->response(null, $e->getMessage(), 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductRequest $request
     * @return JsonResponse
     */
    public function store(ProductRequest $request): JsonResponse
    {
        try {
            $product = new Product();
            $product->name = $request->name;
            $product->description = $request->description;
            $product->price = $request->price;
            $product->sku = $request->sku;
            $product->subcategory_id = $request->subcategory_id;
            $product->category_id = $request->category_id;
            $product->user_upload_id = $request->user()->id;

            if (!$product->save()) {
                return $this->response(null, "Error.", 500);
            }

            foreach ($request->details as $detail){
                $details = new ProductDetails();
                $details->product_id = $product->id;
                $details->key = $detail['key'];
                $details->description = $detail['description'];
                if (!$details->save()) {
                    return $this->response(null, "Error.", 500);
                }
            }

            return $this->response(null, "Success.", 200);
        } catch (\Exception $e) {
            return $this->response(null, $e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product, Request $request)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        if (!$request->user()->authorizeRoles(['admin'])) {
            return $this->response(null, "Unauthorized.", 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
    }
}
