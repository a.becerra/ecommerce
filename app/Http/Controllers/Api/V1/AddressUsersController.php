<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\AddressUsers;
use Illuminate\Http\Request;

class AddressUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AddressUsers  $addressUsers
     * @return \Illuminate\Http\Response
     */
    public function show(AddressUsers $addressUsers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AddressUsers  $addressUsers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AddressUsers $addressUsers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AddressUsers  $addressUsers
     * @return \Illuminate\Http\Response
     */
    public function destroy(AddressUsers $addressUsers)
    {
        //
    }
}
