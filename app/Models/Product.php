<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed|string name
 * @property mixed|string description
 * @property mixed|string price
 * @property mixed|string sku
 * @property mixed|string subcategory_id
 * @property mixed|string category_id
 * @property mixed|string user_upload_id
 * @property mixed id
 */
class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'description',
        'name',
        'price',
        'sku',
        'subcategory_id',
        'category_id',
        'user_upload_id',
    ];

}
