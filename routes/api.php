<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Http\Controllers\Api\V1\UserController as UserV1;

// Products
Route::get('v1/products', [
    App\Http\Controllers\Api\V1\ProductController::class,
    'index'
]);

// Admin
Route::group(['middleware' => ['auth:sanctum', 'admin.role']], function () {
    Route::post('v1/products', [
        App\Http\Controllers\Api\V1\ProductController::class,
        'store'
    ]);
});

// Users
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::resource('v1/users', UserV1::class)
        ->only(['index', 'show']);
});


// Auth
Route::post('v1/login', [
    App\Http\Controllers\Api\V1\Auth\LoginController::class,
    'login'
]);

Route::post('v1/register', [
    App\Http\Controllers\Api\V1\Auth\RegisterController::class,
    'register'
]);
