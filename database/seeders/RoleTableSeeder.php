<?php

namespace Database\Seeders;

use App\Models\Roles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Roles();
        $role->name = 'admin';
        $role->description = 'Administrator';
        $role->save();
        $role = new Roles();
        $role->name = 'user';
        $role->description = 'User';
        $role->save();
        $role = new Roles();
        $role->name = 'client';
        $role->description = 'Cliente';
        $role->save();
    }
}
